import spacy
import ast

from spacy_pattern_builder import build_dependency_pattern, util

from role_pattern_nlp import RolePatternBuilder

from spacy.matcher import DependencyMatcher

import json

corpus = ast.literal_eval(open("./train_patterns.txt").read())

nlp = spacy.load("en_core_web_md")

patterns = []
errs = []

def remapPattern(pattern):
    nodeMap = {}
    k = 1
    for node in pattern:
        nodeMap[node["SPEC"]["NODE_NAME"]] = "node" + str(k)
        k = k + 1
    newPattern = []
    for node in pattern:
        newNode = node.copy()
        newNode["SPEC"] = node["SPEC"].copy()
        newNode["SPEC"]["NODE_NAME"] = nodeMap[newNode["SPEC"]["NODE_NAME"]]
        if "NBOR_NAME" in newNode["SPEC"]:
            newNode["SPEC"]["NBOR_NAME"] = nodeMap[newNode["SPEC"]["NBOR_NAME"]]
        newPattern.append(newNode)
    return newPattern

def havePattern(pat, pats):
    for p in pats:
        if p["pattern"] == pat["pattern"]:
            return True
    return False

def getPatternRecord(inPattern, doc, data):
    pattern = inPattern
    if len(pattern) != len(data["tokens"]):
        print("WARNING: Couldn't create pattern here.")
        return False
    fn = data["fn"]
    matcher = DependencyMatcher(nlp.vocab)                   
    matcher.add("pattern", None, pattern)
    matches = matcher(doc)[0][1][0]
    matches_adj = []
    for m in matches:
        if str(doc[m]) in data["check"]:
            matches_adj.append(m)
    matches = matches_adj
    matched_tokens = [doc[k] for k in matches]
    expected_tokens = [doc[k] for k in data["tokens"]]
    train2pat = {}
    pat2train = {}
    for k in list(range(len(matched_tokens))):
        for j in list(range(len(expected_tokens))):
            if matched_tokens[k] == expected_tokens[j]:
                pat2train[k] = j
                train2pat[j] = k
    fn_adj = {}
    for key, val in fn.items():
        fn_adj[key] = train2pat[val]
    pattern_adj = []
    k = 0
    for node in pattern:
        #{"SPEC": {"NODE_NAME": "node14"}, "PATTERN": {"LEMMA": "be", "POS": "VERB"}}
        node_adj = {}
        node_adj["SPEC"] = node["SPEC"]
        pat_spec = {}
        for key, val in node["PATTERN"].items():
            if key in data["features"][pat2train[k]]:
                pat_spec[key] = val
        node_adj["PATTERN"] = pat_spec
        pattern_adj.append(node_adj)
        k = k + 1
    return {"pattern": pattern_adj, "fn": fn_adj, "txt": data["txt"], "check": data["check"], "tokens": data["tokens"]}

warns = []
for data in corpus:
    doc = nlp(str(data["txt"]))
    check = True
    match_example = {}
    for k in list(range(len(data["tokens"]))):
        match_example[str(k)] = [doc[data["tokens"][k]]]
        if data["check"][k] != str(doc[data["tokens"][k]]):
            check = False
            break
    if not check:
        print("ERROR in pattern, expected %s got %s \n%s\n" % (data["check"][k], str(doc[data["tokens"][k]]), str(data)))
        errs.append("ERROR in pattern, expected %s got %s \n%s\n" % (data["check"][k], str(doc[data["tokens"][k]]), str(data)))
    else:
        print("Matching for %s" % data["txt"])
        pattern = remapPattern(RolePatternBuilder({"DEP": "dep_", "TAG": "tag_", "LEMMA": "lemma_", "POS": "pos_"}).build(match_example).spacy_dep_pattern)
        rec = getPatternRecord(pattern, doc, data)
        if rec:
            if not havePattern(rec, patterns):
                patterns.append(rec)
        else:
            warns.append([data, pattern])

with open("patterns.txt", "w") as outfile:
    for pattern in patterns:
        outfile.write(json.dumps(pattern))
        outfile.write("\n\n")

with open("warnings.txt", "w") as outfile:
    for warn in warns:
        outfile.write(json.dumps(warn))
        outfile.write("\n\n")

with open("errors.txt", "w") as outfile:
    for err in errs:
        outfile.write("%s\n" % err)

