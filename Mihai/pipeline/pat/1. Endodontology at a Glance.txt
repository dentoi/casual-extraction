{"": "The complexity of intracanal infection requires treatment with a broad  spectrum  antibacterial such as sodium  hypochlorite.", "precondition": "treatment with a broad   spectrum   antibacterial such as sodium   hypochlorite ", "action": "The complexity of intracanal infection "}

{"": "Molecular technology enables identification of microorgan- isms without the need for culturing.", "enabled-action": "identification of microorgan- isms ", "condition": "Molecular technology "}

{"": "This enables other bacterial species that would not have survived alone to attach to, and form part of the biofilm.  ", "enabled-action": "other bacterial species that would not have survived alone to attach to ", "condition": "This "}

{"": "If no treatment is provided, the bacteria eventually invade and colonise the pulp.", "event": "no treatment provided ", "condition": "is "}

{"": "If rubber dam is not used, or poor fitting temporary restorations are placed, microleakage can also occur.", "event": "rubber dam not used , or poor fitting temporary restorations are placed ", "condition": "is "}

{"": "However, chronic inflammation can persist if inadequate disinfection is performed, with microorganisms remaining at levels sufficient to stimulate an inflammatory response.", "event": "inadequate disinfection performed , with microorganisms remaining at levels sufficient to stimulate an inflammatory response ", "condition": "is "}

{"": "However, oncethe  microorganisms  penetrate  into  the  inner  dentine  layers, the toxins they produce cause significant pulpal inflammation.", "cause": "the toxins they produce ", "effect": "significant pulpal inflammation "}

{"": "Increased intratubular mineral deposition may also reduce the  permeability of the  dentine  (Figure 1.1).", "effect|reduce": "the   permeability of the   dentine   ( Figure 1.1 ) ", "cause": "Increased intratubular mineral deposition "}

{"": "If no treatment is provided, the bacteria eventually invade and colonise the pulp.", "event": ", the bacteria eventually invade and colonise the pulp . ", "condition": "no treatment is provided "}

{"": "If rubber dam is not used, or poor fitting temporary restorations are placed, microleakage can also occur.", "event": ", microleakage can also occur . ", "condition": "rubber dam is not used , or poor fitting temporary restorations are placed "}

{"": "However, chronic inflammation can persist if inadequate disinfection is performed, with microorganisms remaining at levels sufficient to stimulate an inflammatory response.", "event": "However , chronic inflammation can persist . ", "condition": "inadequate disinfection is performed , with microorganisms remaining at levels sufficient to stimulate an inflammatory response "}

{"": "In addition, various species can produce different enzymes to neutralise the host inflammatory mediators and also inactivate antibacterial solutions that can be used to remove them during root canal treatment.", "cause": "various species ", "hypothetical-effect": "different enzymes "}

{"": "Swelling and pain can result in loss of function of the inflamed area.  ", "cause": "Swelling and pain ", "hypothetical-effect": "loss of function of the inflamed area "}

{"": "In addition, various species can produce different enzymes to neutralise the host inflammatory mediators and also inactivate antibacterial solutions that can be used to remove them during root canal treatment.", "cause": "various species ", "effect": "different enzymes "}

{"": "However, oncethe  microorganisms  penetrate  into  the  inner  dentine  layers, the toxins they produce cause significant pulpal inflammation.", "cause": "the toxins they produce ", "effect": "significant pulpal inflammation "}

{"": "Swelling and pain can result in loss of function of the inflamed area.  ", "cause": "Swelling and pain ", "effect": "loss of function of the inflamed area "}

{"": "The injury also results in pulpal necrosis and  subsequent bacterial infection.", "cause": "The injury ", "effect": "pulpal necrosis and   subsequent bacterial infection "}

{"": "Internal replacement resorption occurs when hard tissue resembling bone (osteoid) or cementum is deposited (reparative phase) within the resorptive cavity.  ", "circumstance": "hard tissue resembling bone ( osteoid ) or cementum is deposited ( reparative phase ) within the resorptive cavity ", "event": "Internal replacement resorption occurs .  "}

{"": "However, as the  immune  challenge increases, the  pulpal  damage  will advance beyond  repair,  resulting  in irreversible inflammation and progressive pulpal necrosis.  ", "cause": "However , as the   immune   challenge increases , the   pulpal   damage   will advance beyond   repair ,   .  ", "effect": "irreversible inflammation and progressive pulpal necrosis "}

{"": "The odontoblast/predentine  layers of the pulpo-dentine complex must be damaged resulting in exposure of mineralised dentine.", "cause": "The odontoblast / predentine   layers of the pulpo - dentine complex must be damaged . ", "effect": "exposure of mineralised dentine "}

{"": "Swelling is caused by the accumulation of tissue exudates which contain neutrophils  and inflammatory mediators (Table 1.1).", "cause": "the accumulation of tissue exudates which contain neutrophils   and inflammatory mediators ( Table 1.1 ) ", "effect": "Swelling "}

{"": "Apical periodontitis is caused by bacterial infection of the pulp.  ", "cause": "bacterial infection of the pulp ", "effect": "Apical periodontitis "}

{"": "Apical periodontitis is caused by the presence of microorganisms and their toxins in the root canals causing progressive inflammation and necrosis of the pulp, followed by inflammation of the periapical tissues.", "cause": "the presence of microorganisms and their toxins in the root canals causing progressive inflammation and necrosis of the pulp ", "effect": "Apical periodontitis "}

{"": "Pulpal inflammation  can initially be reversible, with removal of the irritants resulting in resolution of the inflammation.  ", "cause": "the irritants ", "effect": "resolution of the inflammation "}

{"": "In addition, various species can produce different enzymes to neutralise the host inflammatory mediators and also inactivate antibacterial solutions that can be used to remove them during root canal treatment.", "goal": "neutralise the host inflammatory mediators ", "action": "In addition , various species can produce different enzymes and also inactivate antibacterial solutions that can be used to remove them during root canal treatment . "}

{"": "calcium hydroxide medicament can be placed to exert an antibacterial effect on tissue inaccessible to instrumentation.  ", "goal": "exert an antibacterial effect on tissue inaccessible to instrumentation ", "action": "calcium hydroxide medicament can be placed .  "}

