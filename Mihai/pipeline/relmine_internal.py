import ast
import spacy
import string

import json

from spacy.matcher import DependencyMatcher

from os import listdir
from os.path import isfile, join

import sys

def getSubtree(doc, iStart, match):
    subtree = [iStart]
    toVisit = [c.i for c in doc[iStart].children]
    while toVisit:
        i = toVisit.pop()
        if i not in match:
            subtree.append(i)
            toVisit = toVisit + [c.i for c in doc[i].children]
    subtree.sort()
    return subtree

patterns = [ast.literal_eval(p) for p in open("./patterns.txt").read().splitlines() if p.strip() != ""]

rels = []

fileName = sys.argv[1]

infile = join("./ctxt", fileName)
outfile = join("./pat/", fileName)
logfile = join("./log/", fileName)

print("Mining ... %s" % infile)
nlp = spacy.load("en_core_web_md", disable=["ner"])
#nlp = spacy.load(r"C:\Users\time\anaconda3\Lib\site-packages\en_core_web_md\en_core_web_md-2.3.1", disable=["ner"])
nlp.max_length = 3800000
raw = str(open(infile).read())
#raw = raw.replace("-\n", "")
#raw = raw.replace("\"", "")
#raw = raw.replace("“", "")
#raw = raw.replace("”", "")
#raw = raw.replace("- \n", "")
#raw = raw.replace("- ", "")

doc = nlp(raw)

p = 0
maxJ = len(list(doc.sents))
for pattern in patterns:
    p = p + 1
    j = 0
    matcher = DependencyMatcher(nlp.vocab)
    matcher.add("pattern", None, pattern["pattern"])
    for sent in doc.sents:
        j = j + 1
        print("\t\t%d/%d patterns, %d/%d sents done; %d rels found" % (p, len(patterns), j, maxJ, len(rels)), end="\r")
        #docM = nlp(str(sent))
        docM = sent.as_doc()
        matches = matcher(docM)[0][1]
        for match in matches:
            rel = {"": str(docM[match[0]].sent).translate({ord(i):" " for i in '\n'})}
            for k, v in pattern["fn"].items():
                bits = [str(docM[i]) for i in getSubtree(docM, match[v], match)]
                s = ""
                for b in bits:
                    s = s + b.translate({ord(i):None for i in '\n'}) + " "
                rel[k] = s
            rels.append(rel)
print("Mining done.")

with open(outfile, "w") as outf:
    for rel in rels:
        outf.write(json.dumps(rel))
        outf.write("\n\n")

with open(logfile, "w") as outfile:
    for rel in rels:
        outfile.write("%s\n" % rel[""])
        for k in sorted(rel.keys()):
            if k != "":
                outfile.write("\t%s:\t%s\n" % (k, rel[k]))
        outfile.write("\n")

