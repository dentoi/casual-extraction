import os
from os import listdir
from os.path import isfile, join

from random import shuffle

infiles = [f for f in listdir("./ctxt") if isfile(join("./ctxt", f))]
#infiles = ["dbgpat.txt"]
shuffle(infiles)

for f in infiles:
    os.system('python ./relmine_internal.py \"%s\"' % f)

