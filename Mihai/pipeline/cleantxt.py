import os
import sys

from os import listdir
from os.path import isfile, join

from random import shuffle

infiles = [f for f in listdir("./txt") if isfile(join("./txt", f))]
#infiles = ["39. ENDODONTICS PRINCIPLES AND PRACTICE 5th edition_part2.txt"]

def cleanSqBs(l):
    while True:
        left = l.find("[")
        if -1 == left:
            break
        right = l[left:].find("]")
        lstr = l[:left]
        rstr = l[left + right + 1:]
        l = lstr + rstr
    return l.replace("]", "")

for f in infiles:
    # print(f)


    with open(join('./txt/',f), 'r', encoding='utf-8') as z:
        content = z.readlines()
    # you may also want to remove whitespace characters like `\n` at the end of each line
    all_lines = [x.strip() for x in content]
    # print(content)



    # all_lines = [cleanSqBs(x.strip()) for x in open(join("./txt", f),  encoding='utf-8' ).read().splitlines() if x.strip()]


    # print(all_lines)
    txt = all_lines
    ctxt = []
    prep = ""
    count = 0;
    for i, l in enumerate(txt):
        # print(l)
        if l:
            if l[-1] in [".", ":", "?", "!"]:
                if l[0].isupper():
                    prep = ""
                ctxt.append(prep + l)
                prep = ""
            else:
                if l[0].isupper():
                    prep = ""
                prep = prep + l + " "
    with open(join("./ctxt", f), "w") as outfile:
        for l in ctxt:
            try:
                outfile.write("%s\n\n" % l)
            except:
                count+=1

        print('finish', outfile.name, 'error', count)

